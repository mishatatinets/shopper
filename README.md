# Shopper Open-Source Shopping Cart

Shopper is an open source shopping cart created to demonstrate what students of the [Web Development From Scratch](https://www.codemy.net/courses/web-development-from-scratch) course.

## Installation

Please see this [playlist](http://www.youtube.com/playlist?list=PLjQo0sojbbxUav7I746f0lT4apGX8-iON) on youtube for tutorial on how to deploy a rails app.